#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include "util.h"


void test1(){
    printf("Test1\n");
    printf("Just a successful memory allocation with release\n");
    void* ptr_on_heap = heap_init(40000);
    if (!ptr_on_heap){
        err("Can't create heap\n");
    }
    debug_heap(stdout,ptr_on_heap);

    void* ptr_on_data = _malloc(3200);
    if (!ptr_on_data){
        err("Error in allocation 1\n");
    }
    debug_heap(stdout, ptr_on_heap);
    _free(ptr_on_data);
    munmap(ptr_on_heap, size_from_capacity((block_capacity) {.bytes = 40000}).bytes);
    printf("Success\n");
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
}

void test2(){
    printf("Test2\n");
    printf("Memory allocation of two blocks and realise one of them\n");
    void* ptr_on_heap = heap_init(40000);
    if (!ptr_on_heap){
        err("Can't create heap\n");
    }
    debug_heap(stdout,ptr_on_heap);

    void* ptr_on_data1 = _malloc(3200);
    if (!ptr_on_data1){
        err("Error in allocation 1\n");
    }
    debug_heap(stdout, ptr_on_heap);

    void* ptr_on_data2 = _malloc(4597);
    if (!ptr_on_data2){
        err("Error in allocation 2\n");
    }
    debug_heap(stdout,ptr_on_heap);

    _free(ptr_on_data2);
    debug_heap(stdout, ptr_on_heap);
    _free(ptr_on_data1);
    munmap(ptr_on_heap, size_from_capacity((block_capacity) {.bytes = 40000}).bytes);
    printf("Success\n");
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
}


void test3(){
    printf("Test3\n");
    printf("Memory allocation of two blocks and realise them\n");
    void* ptr_on_heap = heap_init(40000);
    if (!ptr_on_heap){
        err("Can't create heap\n");
    }
    debug_heap(stdout,ptr_on_heap);

    void* ptr_on_data1 = _malloc(3200);
    if (!ptr_on_data1){
        err("Error in allocation 1\n");
    }
    debug_heap(stdout, ptr_on_heap);

    void* ptr_on_data2 = _malloc(4597);
    if (!ptr_on_data2){
        err("Error in allocation 2\n");
    }
    debug_heap(stdout,ptr_on_heap);

    _free(ptr_on_data2);
    _free(ptr_on_data1);
    debug_heap(stdout, ptr_on_heap);
    munmap(ptr_on_heap, size_from_capacity((block_capacity) {.bytes = 40000}).bytes);
    printf("Success\n");
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
}

void test4(){
    printf("Test4\n");
    printf("Extension of the old memory space\n");
    void* ptr_on_heap = heap_init(40000);
    if (!ptr_on_heap){
        err("Can't create heap\n");
    }
    debug_heap(stdout,ptr_on_heap);

    void* ptr_on_data1 = _malloc(30000);
    if (!ptr_on_data1){
        err("Error in allocation 1\n");
    }
    debug_heap(stdout, ptr_on_heap);

    void* ptr_on_data2 = _malloc(20000);
    if (!ptr_on_data2){
        err("Error in allocation 2\n");
    }
    debug_heap(stdout,ptr_on_heap);
    _free(ptr_on_data2);
    _free(ptr_on_data1);
    munmap(ptr_on_heap, size_from_capacity((block_capacity) {.bytes = 40000}).bytes);
    printf("Success\n");
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
}




void test5(){
    printf("When there's not enough space, the creation of a region happen in the other place, so there is not any extension of an old one\n");
    void* ptr_on_heap = heap_init(40000);
    if (!ptr_on_heap){
        err("Can't create heap\n");
    }
    debug_heap(stdout, ptr_on_heap);

    (void) mmap(ptr_on_heap+REGION_MIN_SIZE, REGION_MIN_SIZE,PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);


    void *ptr_on_data = _malloc(60000);
    if (!ptr_on_data) {
        err("Error in allocation 1\n");
    }
    debug_heap(stdout, ptr_on_heap);
    _free(ptr_on_data);
    debug_heap(stdout, ptr_on_heap);
    munmap(ptr_on_heap, REGION_MIN_SIZE);
    munmap(ptr_on_heap, size_from_capacity((block_capacity) {.bytes = 40000}).bytes);
    printf("Success\n");
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
}

void check() {
    test1();
    test2();
    test3();
    test4();
    test5();
}
